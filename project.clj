(defproject glitter "0.1.0-SNAPSHOT"
            :description "Glitter: Git Uninterrupted"
            :url "http://github.com/CraZySacX/glitter"
            :license {:name "Eclipse Public License"
                      :url  "http://www.eclipse.org/legal/epl-v10.html"}
            :source-paths ["src/clj" "src/cljs"]
            :test-paths ["spec/clj"]
            :dependencies [[org.clojure/clojure "1.7.0-master-SNAPSHOT"]
                           [org.clojure/clojurescript "0.0-2356"]
                           [org.clojure/core.async "0.1.346.0-17112a-alpha"]
                           [org.clojure/tools.cli "0.3.1"]
                           [org.clojure/tools.nrepl "0.2.6"]
                           [cljs-ajax "0.3.3"]
                           [compojure "1.2.0"]
                           [com.cemerick/friend "0.2.1"]
                           [com.datomic/datomic-pro "0.9.4899"]
                           [environ "1.0.0"]
                           [fogus/ring-edn "0.2.0"]
                           [kioo "0.4.1-SNAPSHOT"]
                           [om "0.7.3"]
                           [om-sync "0.1.1"]
                           [org.clojars.franks42/cljs-uuid-utils "0.1.3"]
                           [org.ozias.cljlibs/logging "0.1.5"]
                           [ring "1.3.1"]
                           [sablono "0.2.22"]]
            :profiles {:dev  {:dependencies [[org.clojure/tools.namespace "0.2.7"]
                                             [clojure-complete "0.2.4"]
                                             [com.cemerick/piggieback "0.1.3"]
                                             [lein-light-nrepl "0.0.18"]
                                             [org.ozias.cljlibs/scm "0.1.3"]
                                             [ring-server "0.3.1"]
                                             [speclj "3.1.0"]]
                              :plugins      [[lein-cljsbuild "1.0.3"]
                                             [lein-environ "1.0.0"]
                                             [lein-ring "0.8.12"]
                                             [org.ozias.plugins/lein-git-version "1.1.4"]
                                             [speclj "3.1.0"]]
                              :source-paths ["src/local/clj" "src/local/cljs" "dev"]
                              :repl-options {:nrepl-middleware [cemerick.piggieback/wrap-cljs-repl]}
                              :env          {:datomic-url "datomic:mem://glitter"
                                             :create true}}
                       :qa   {:source-paths ["src/qa/clj" "src/qa/cljs"]
                              :env          {:datomic-url ""}}
                       :prod {:source-paths ["src/prod/clj" "src/prod/cljs"]
                              :env          {:datomic-url ""}}}
            :cljsbuild {:builds {:local {:source-paths ["src/cljs" "src/local/cljs" "spec/cljs"]
                                         :compiler     {:output-to     "resources/public/js/glitter.js"
                                                        :optimizations :simple
                                                        :pretty-print  true
                                                        :preamble      ["react/react.js"]
                                                        :externs       ["react/externs/react.js"]}}
                                 :qa    {:source-paths ["src/cljs" "src/qa/cljs"]
                                         :compiler     {:output-to     "resources/public/js/glitter.js"
                                                        :optimizations :whitespace
                                                        :pretty-print  true}}
                                 :prod  {:source-paths ["src/cljs" "src/prod/cljs"]
                                         :compiler     {:output-to     "resources/public/js/glitter.js"
                                                        :optimizations :advanced}}}}
            :aliases {"local-clean" ["cljsbuild" "clean"]
                      "local-build" ["cljsbuild" "once" "local"]
                      "local"       ["do" "local-clean," "local-build"]
                      "qa-clean"    ["with-profile" "+qa" "cljsbuild" "clean"]
                      "qa-build"    ["with-profile" "+qa" "cljsbuild" "once" "qa"]
                      "qa"          ["do" "qa-clean," "qa-build"]
                      "prod-clean"  ["with-profile" "+prod" "cljsbuild" "clean"]
                      "prod-build"  ["with-profile" "+prod" "cljsbuild" "once" "prod"]
                      "prod"        ["do" "prod-clean," "prod-build"]}
            :repositories {"snapshots"      {:url "http://www.ozias.net/artifactory/libs-snapshot-local"}
                           "releases"       {:url "http://www.ozias.net/artifactory/libs-release-local"}
                           "my.datomic.com" {:url "https://my.datomic.com/repo" :creds :gpg}}
            :ring {:handler glitter.core/app}
            :deploy-repositories
            [["snapshots"
              {:url   "http://www.ozias.net/artifactory/libs-snapshot-local"
               :creds :gpg}]
             ["releases"
              {:url   "http://www.ozias.net/artifactory/libs-release-local"
               :creds :gpg}]]
            :scm {:name "git"
                  :url  "https://github.com/CraZySacX/glitter"}
            :manifest {"Implementation-Version" "0.1.0-SNAPSHOT"}
            :git-version {:file {:assoc-in-keys [[:manifest "Implementation-Version"]]}})
