(ns glitter.spec-utils)

(defn request [type resource web-app & params]
  (web-app {:request-method type :uri resource :params (first params)}))

(defn get-id [x _]
  {:current x
   :authentications {x {:roles #{(keyword "role" x)}}}})

(defmacro as-x [x & body]
  `(with-redefs [cemerick.friend/identity (partial get-id ~x)]
     ~@body))

(defmacro as-admin [& body]
  `(as-x "admin" ~@body))

(defmacro as-user [& body]
  `(as-x "user" ~@body))