(ns glitter.account-spec
  (:require [clojure.tools.reader.edn :refer [read-string]]
            [glitter.account :refer [base-account-routes]]
            [glitter.util :refer [logging-options]]
            [glitter.entity.account :refer [uem->um]]
            [glitter.entity.core :refer :all]
            [glitter.spec-utils :refer :all]
            [org.ozias.cljlibs.logging.logging :refer [configure-logging]]
            [speclj.core :refer :all])
  (:refer-clojure :exclude [read read-string]))

(defn- account-post []
  (request :post "/account/test" base-account-routes {:password "test"}))

(defn- accounts-get []
  (request :get "/account" base-account-routes))

(defn- accounts-history-get []
  (request :get "/account/history" base-account-routes))

(defn- account-get [username]
  (request :get (str "/account/" username) base-account-routes))

(defn- account-put [username]
  (request :put (str "/account/" username) base-account-routes
           {:password "test"
            :roles    [:role/user]}))

(defn root-eid []
  (->> (accounts-history-get)
       as-admin
       :body
       read-string
       :accounts-history
       (filter #(= "root" (second %)))
       first
       last))

(describe
  "account"
  (before-all (configure-logging
                {:options (assoc logging-options :verbosity 2)}))
  (context
    "uem->m"
    (before-all (start-datomic))
    (after-all (println))
    (with-all um (uem->um))
    (it "should be a map" (should-be map? @um))
    (it "should contain a valid user" (should-contain "root" @um))
    (it "should not contain an invalid user" (should-not-contain "blah" @um))
    (it "should be a map at 'user'" (should-be map? (get @um "root")))
    (with root (get @um "root"))
    (it "should contain a username" (should-contain :username @root))
    (it "should contain a password" (should-contain :password @root))
    (it "should contain roles" (should-contain :roles @root))
    (with roles (get @root :roles))
    (it "should contain a valid role" (should-contain :role/user @roles))
    (it "should not contain an invalid role"
        (should-not-contain :role/yoda @roles)))

  (context
    "/account POSTs"
    (before-all (start-datomic))
    (after-all (println))
    (with-all admin-resp (as-admin (account-post)))
    (with-all user-resp (as-user (account-post)))
    (after-all (as-admin (request :delete "/account/test" base-account-routes)))

    ;response
    (it "should be a map" (should-be map? @admin-resp))

    ;status
    (it "should contain :status" (should-contain :status @admin-resp))
    (with status (:status @admin-resp))
    (it "should have a status of 200" (should= 200 @status))

    ; headers
    (it "should contain :headers" (should-contain :headers @admin-resp))
    (with headers (:headers @admin-resp))
    (it "should be a map" (should-be map? @headers))
    (it "should contain 'Content-Type'"
        (should-contain "Content-Type" @headers))
    (it "should be edn"
        (should= "application/edn" (get @headers "Content-Type")))

    ; body
    (it "should contain :body" (should-contain :body @admin-resp))
    (it "should be a string" (should-be string? (:body @admin-resp)))

    ; unauthorized account
    (it "should not be authorized" (should-throw Exception @user-resp)))

  (context
    "/account GETs"
    (before-all (start-datomic))
    (after-all (println))
    (with-all admin-resp (as-admin (accounts-get)))
    (with-all user-resp (as-user (accounts-get)))

    ; response
    (it "should be a map" (should-be map? @admin-resp))

    ; status
    (it "should contain :status" (should-contain :status @admin-resp))
    (with status (:status @admin-resp))
    (it "should have a status of 200" (should= 200 @status))

    ; headers
    (it "should contain :headers" (should-contain :headers @admin-resp))
    (with headers (:headers @admin-resp))
    (it "should be a map" (should-be map? @headers))
    (it "should contain 'Content-Type'"
        (should-contain "Content-Type" @headers))
    (it "should be edn"
        (should= "application/edn" (get @headers "Content-Type")))

    ; body
    (it "should contain :body" (should-contain :body @admin-resp))
    (it "should be a string" (should-be string? (:body @admin-resp)))

    ; unauthorized account
    (it "should not be authorized" (should-throw Exception @user-resp)))

  (context
    "/account/history GETs"
    (before-all (start-datomic))
    (after-all (println))
    (with-all admin-resp (as-admin (accounts-history-get)))
    (with-all user-resp (as-user (accounts-history-get)))

    ; response
    (it "should be a map" (should-be map? @admin-resp))

    ; status
    (it "should contain :status" (should-contain :status @admin-resp))
    (with status (:status @admin-resp))
    (it "should have a status of 200" (should= 200 @status))

    ; headers
    (it "should contain :headers" (should-contain :headers @admin-resp))
    (with headers (:headers @admin-resp))
    (it "should be a map" (should-be map? @headers))
    (it "should contain 'Content-Type'"
        (should-contain "Content-Type" @headers))
    (it "should be edn"
        (should= "application/edn" (get @headers "Content-Type")))

    ; body
    (it "should contain :body" (should-contain :body @admin-resp))
    (it "should be a string" (should-be string? (:body @admin-resp)))

    ; unauthorized account
    (it "should not be authorized" (should-throw Exception @user-resp)))

  (context
    "/account/:eid GETs"
    (before-all (start-datomic))
    (after-all (println))
    (with-all response (as-admin (account-get (root-eid))))
    (with-all user-resp (as-user (account-get (root-eid))))

    ; response
    (it "should be a map" (should-be map? @response))

    ; status
    (it "should contain :status" (should-contain :status @response))
    (with status (:status @response))
    (it "should have a status of 200" (should= 200 @status))

    ; headers
    (it "should contain :headers" (should-contain :headers @response))
    (with headers (:headers @response))
    (it "should be a map" (should-be map? @headers))
    (it "should contain 'Content-Type'"
        (should-contain "Content-Type" @headers))
    (it "should be edn"
        (should= "application/edn" (get @headers "Content-Type")))

    ; body
    (it "should contain :body" (should-contain :body @response))
    (it "should be a string" (should-be string? (:body @response)))

    ; unauthorized account
    (it "should not be authorized" (should-throw Exception @user-resp)))

  (context
    "/account/:username GETs"
    (before-all (start-datomic))
    (after-all (println))
    (with-all response (as-x "root" (account-get "root")))
    (with-all user-resp (as-user (account-get "root")))

    ; response
    (it "should be a map" (should-be map? @response))

    ; status
    (it "should contain :status" (should-contain :status @response))
    (with status (:status @response))
    (it "should have a status of 200" (should= 200 @status))

    ; headers
    (it "should contain :headers" (should-contain :headers @response))
    (with headers (:headers @response))
    (it "should be a map" (should-be map? @headers))
    (it "should contain 'Content-Type'"
        (should-contain "Content-Type" @headers))
    (it "should be edn"
        (should= "application/edn" (get @headers "Content-Type")))

    ; body
    (it "should contain :body" (should-contain :body @response))
    (it "should be a string" (should-be string? (:body @response)))

    ; unauthorized account
    (it "should not be authorized" (should-throw Exception @user-resp))))
