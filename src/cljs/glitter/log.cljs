(ns glitter.log)

(defn log [msg]
  (.log js/console msg))

(defn error [msg]
  (.error js/console msg))

(defn info [msg]
  (.info js/console msg))

(defn warn [msg]
  (.warn js/console msg))

