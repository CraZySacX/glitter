(ns glitter.siren
  (:require [cljs.core.async :refer [<! >! chan sub]]
            [cljs-uuid-utils :refer [make-random-uuid uuid-string]]
            [glitter.effects :refer [fade-in fade-out]]
            [glitter.pub :refer [publication publisher]]
            [goog.dom :as gdom]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(def siren-event {:topic :siren-event})

(defn siren [m msg]
  (go (>! publisher (assoc m :message msg))))

(defn siren-danger [msg & sticky]
  (siren (assoc siren-event :style "alert-danger" :sticky sticky) msg))

(defn siren-warn [msg & sticky]
  (siren (assoc siren-event :style "alert-warning" :sticky sticky) msg))

(defn siren-info [msg & sticky]
  (siren (assoc siren-event :style "alert-info" :sticky sticky) msg))

(defn siren-success [msg & sticky]
  (siren (assoc siren-event :style "alert-success" :sticky sticky) msg))

(defn siren-message-view [message _]
  (reify
    om/IRender
    (render [_]
      (let [base ["pull-right alert"]
            base (conj base (or (:style message) "alert-success"))
            base (conj base (if (:sticky message) "alert-dismissable"))
            base (remove nil? base)
            clz (apply str (interpose " " base))
            m (:message message)]
        (dom/div #js {:id        (:uuid message)
                      :className (str "siren-message " clz)
                      :style #js {:clear "right"}}
                 (if (:sticky message)
                   (dom/button #js {:className    "close"
                                    :data-dismiss "alert"}
                               (dom/span #js {:aria-hidden "true"} "x")
                               (dom/span #js {:className "sr-only"} "Close")))
                 (if (string? m) m)
                 (if (:pre m)
                   (dom/pre #js {:className "sm"} (:text m))))))))

(defn siren-container-view [_ owner]
  (reify
    om/IInitState
    (init-state [_] {:siren-chan (chan)
                     :messages   []
                     :displayed  []})
    om/IWillMount
    (will-mount [_]
      (let [c (om/get-state owner :siren-chan)]
        (sub publication :siren-event c)
        (go (loop []
              (when-let [e (<! c)]
                (let [messages (om/get-state owner :messages)
                      e (assoc e :uuid (uuid-string (make-random-uuid)))]
                  (om/update-state!
                    owner (fn [state] (assoc state :messages (conj messages e)))))
                (recur))))))
    om/IDidUpdate
    (did-update [_ _ _]
      (doseq [m (om/get-state owner :messages)]
        (let [uuid (:uuid m)
              node (gdom/getElement uuid)
              d (om/get-state owner :displayed)]
          (when-not (some #{uuid} d)
            ((fade-in) node)
            (if-not (:sticky m)
              (js/setTimeout #((fade-out 1000 (fn [] (gdom/removeNode node))) node) 5000))
            (om/update-state! owner (fn [state] (assoc state :displayed (conj d uuid))))))))
    om/IRenderState
    (render-state [_ state]
      (apply dom/div #js {:id        "siren-container"
                          :style     #js {:position "fixed"
                                          :bottom   "10px"
                                          :right    "10px"}}
             (om/build-all siren-message-view (:messages state))))))