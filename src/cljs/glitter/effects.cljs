(ns glitter.effects
  (:require [goog.events :as events]
            [goog.fx :as fx]
            [goog.fx.dom :as fx-dom]))

(defn fade-out
  ([] (fade-out 1000 nil))
  ([tm] (fade-out tm nil))
  ([tm callback]
   (fn [node]
     (let [anim (fx-dom/FadeOut. node tm)]
       (when callback
         (events/listen anim js/goog.fx.Animation.EventType.END callback))
       (. anim (play))))))

(defn fade-in
  ([] (fade-in 1000 nil))
  ([tm] (fade-in tm nil))
  ([tm callback]
   (fn [node]
     (let [anim (fx-dom/FadeIn. node tm)]
       (when callback
         (events/listen anim js/goog.fx.Animation.EventType.END callback))
       (. anim (play))))))