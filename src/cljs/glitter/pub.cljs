(ns glitter.pub
  (:require [cljs.core.async :refer [chan pub]]))

(def publisher (chan))
(def publication (pub publisher :topic))