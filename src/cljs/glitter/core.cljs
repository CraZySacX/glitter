(ns glitter.core
  (:require [clojure.string :refer [replace]]
            [ajax.core :refer [GET POST]]
            [cljs.core.async :refer [<! >! chan pub put! sub]]
            [glitter.log :refer [error log]]
            [glitter.nav :refer [nav-buttons-view]]
            [glitter.pub :refer [publication publisher]]
            [glitter.siren :as siren]
            [goog.dom :as gdom]
            [goog.dom.forms :as f]
            [goog.events :as events]
            [kioo.core :refer [handle-wrapper]]
            [kioo.om :refer [append content do-> listen set-attr substitute]]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true]
            [om-sync.core :refer [om-sync]]
            [om-sync.util :refer [tx-tag edn-xhr]])
  (:require-macros [cljs.core.async.macros :refer [go]]
                   [kioo.om :refer [defsnippet deftemplate]]))

(enable-console-print!)

(def ENTER_KEY 13)

(def app-state (atom {}))

(def base-handler-config {:handler       (partial log)
                          :error-handler (partial error)})

(defn get-lang []
  (replace
    (let [nav js/navigator]
      (or (first (.-languages nav))
          (.-language nav)
          (.-userLanguage nav)))
    #"-" "_"))

(defn admin []
  (GET "/admin" base-handler-config))

(defn config []
  (GET "/config" base-handler-config))

(defn app-state-handler [res]
  (reset! app-state res))

(defn auth-event-handler [res]
  (GET (str "/app-state/" (get-lang)) {:handler       (partial app-state-handler)
                                       :error-handler (partial error)})
  (go (>! publisher {:topic :auth-event :res res})))

(defn logout []
  (POST "/logout" {:handler       (partial auth-event-handler)
                   :error-handler (partial auth-event-handler)}))

(defn login []
  (POST "/login" {:handler       (partial auth-event-handler)
                  :error-handler (partial auth-event-handler)
                  :params        {:username (f/getValue (gdom/getElement "username"))
                                  :password (f/getValue (gdom/getElement "password"))}})
  (f/setValue (gdom/getElement "username") "")
  (f/setValue (gdom/getElement "password") ""))

(defn drop-until
  "Returns a lazy sequence of the items in coll starting from the first
item after which (pred item) returns logical true."
  {:added  "1.7"
   :static true}
  [pred coll]
  (let [step (fn [pred coll]
               (when-let [s (seq coll)]
                 (if (pred (first s))
                   (rest s)
                   (recur pred (rest s)))))]
    (lazy-seq (step pred coll))))

(defn version-only [app-state]
  (apply str (take-while #(not= \+ %) (:app-state/version app-state))))

(defn semver-metadata [app-state]
  (apply str (drop-until #(= \+ %) (:app-state/version app-state))))

(defn sha-only [app-state]
  (apply str (take-while #(not= \. %) (semver-metadata app-state))))

(defn date-only [app-state]
  (apply str (drop-until #(= \. %) (semver-metadata app-state))))

(defsnippet nav "templates/nav.html" [:nav]
            [app-state]
            {[:.navbar-brand] (do-> (content (-> app-state :app-state/localized :localized/brand))
                                    (set-attr :title (version-only app-state))
                                    (listen :onClick #(siren/siren-success
                                                       {:pre  true
                                                        :text (str
                                                                "Version: " (version-only @app-state) "\n"
                                                                "SHA:     " (sha-only @app-state) "\n"
                                                                "Date:    " (date-only @app-state))})))
             [:.navbar-right] (append (om/build nav-buttons-view app-state))})

(defsnippet container "templates/container.html" [:.container-fluid]
            [app-state]
            {})

(defsnippet login-modal "templates/login-modal.html" [:.modal]
            [app-state]
            {[:#login] (listen :onClick #(login))})

(defsnippet logout-modal "templates/logout-modal.html" [:.modal]
            [app-state]
            {[:#logout-confirm] (listen :onClick #(logout))})

(deftemplate index-page "public/html/index.html"
             [app-state]
             {[:nav]             (substitute (nav app-state))
              [:container]       (substitute (container app-state))
              [:login-modal]     (substitute (login-modal app-state))
              [:logout-modal]    (substitute (logout-modal app-state))
              [:siren-container] (substitute (om/build siren/siren-container-view app-state))})

(defn app-view [app-state] (om/component (index-page app-state)))

(let [tx-chan (chan)
      tx-pub-chan (pub tx-chan (fn [_] :txs))]
  (edn-xhr
    {:method :get
     :url    (str "/app-state/" (get-lang))
     :on-complete
             (fn [res]
               (reset! app-state res)
               (om/root app-view app-state
                        {:target (gdom/getElement "app")
                         :shared {:tx-chan tx-pub-chan}
                         :tx-listen
                                 (fn [tx-data root-cursor]
                                   (put! tx-chan [tx-data root-cursor]))}))}))
