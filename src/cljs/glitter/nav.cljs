(ns glitter.nav
  (:require [cljs.core.async :refer [<! >! chan pub put! sub]]
            [ajax.core :refer [GET]]
            [glitter.log :refer [log error]]
            [glitter.pub :refer [publication]]
            [glitter.siren :as siren]
            [om.core :as om :include-macros true]
            [om.dom :as dom :include-macros true])
  (:require-macros [cljs.core.async.macros :refer [go]]))

(def ^{:private true} lim "#login-modal")
(def ^{:private true} lom "#logout-modal")
(def ajax-h-config {:handler       #(log (str %))
                    :error-handler #(error (str %))})

(defn in? [xs x]
  (some #(= x %) xs))

(defn ls [app k]
  (-> app :app-state/localized k))

(defn get-roles [app]
  (-> app :app-state/identity :authentications first second :roles))

(defn roles? [app]
  (seq (get-roles app)))

(defn get-accounts [& {:keys [type]}]
  (condp = type
    :hist (GET "/account/history" ajax-h-config)
    (GET "/account" ajax-h-config)))

(defn toggle-login-btn [app state]
  (let [login (ls app :localized/login)
        logout (ls app :localized/logout)]
    (assoc state :curr-text
                 (if (= (:curr-text state) login) logout login)
                 :curr-target
                 (if (= (:curr-target state) lim) lom lim))))

(defn nav-buttons-view [app owner]
  (reify
    om/IInitState
    (init-state [_] {:auth-chan   (chan)
                     :curr-text   (if (roles? app)
                                    (ls app :localized/logout)
                                    (ls app :localized/login))
                     :curr-target (if (roles? app) lom lim)})
    om/IWillMount
    (will-mount [_]
      (let [c (om/get-state owner :auth-chan)]
        (sub publication :auth-event c)
        (go (loop []
              (when-let [result (:res (<! c))]
                (if (contains? result :auth)
                  (let [m (:message result)
                        text (ls @app m)]
                    (om/update-state! owner (partial toggle-login-btn @app))
                    (siren/siren-success text)))
                (when (= 401 (:status result))
                  (let [m (-> result :response :message)
                        text (ls @app m)]
                    (siren/siren-danger text true)))
                (recur))))))
    om/IRenderState
    (render-state [_ state]
      (let [roles (get-roles app)]
        (dom/div #js {:className "btn-group"}
                 (if (in? roles :role/admin)
                   (dom/div #js {:className "btn-group"}
                            (dom/button #js {:id          "navbar-admin-btn"
                                             :className   "btn btn-primary navbar-btn dropdown-toggle"
                                             :data-toggle "dropdown"}
                                        (str (ls app :localized/admin) " ")
                                        (dom/span #js {:className "caret"}))
                            (dom/ul #js {:className "dropdown-menu"
                                         :role      "menu"}
                                    (dom/li nil (dom/a #js {:href "#"} "User Management"))
                                    (dom/li nil (dom/a #js {:href "#"} "Repo Management")))))
                 (if (or (in? roles :role/admin)
                         (in? roles :role/user))
                   (dom/div #js {:className "btn-group"}
                            (dom/button #js {:id          "navbar-admin-btn"
                                             :className   "btn btn-primary navbar-btn dropdown-toggle"
                                             :data-toggle "dropdown"}
                                        (str (ls app :localized/account) " ")
                                        (dom/span #js {:className "caret"}))
                            (dom/ul #js {:className "dropdown-menu"
                                         :role      "menu"}
                                    (dom/li nil (dom/a #js {:onClick (partial get-accounts)} "Accounts"))
                                    (dom/li nil (dom/a #js {:onClick (partial get-accounts :type :hist)} "Accounts History"))
                                    (dom/li nil (dom/a #js {:href "#"} "Profile"))
                                    (dom/li nil (dom/a #js {:href "#"} "SSH Keys")))))
                 (dom/button #js {:id          "navbar-login-btn"
                                  :className   "btn btn-primary navbar-btn"
                                  :data-toggle "modal"
                                  :data-target (:curr-target state)}
                             (:curr-text state)))))))