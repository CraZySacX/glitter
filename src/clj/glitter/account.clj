(ns glitter.account
  "Account REST API"
  (:require [cemerick.friend :refer [authorize]]
            [compojure.core :refer [DELETE GET POST PUT context
                                    defroutes]]
            [glitter.entity.account :refer :all]
            [glitter.entity.util :refer [get-by-val]]
            [glitter.util :refer :all])
  (:import (java.util.concurrent ExecutionException)))

(defn- create-account
  "Create an account with the given unique username.

  Evaluates to an EDN response."
  [username password]
  (if-not (ffirst (get-by-val :account/user username))
    (try
      (let [res @(create-account-entity username password)]
        (if (contains? res :tx-data)
          (generate-edn-response (str "Account created successfully."))
          (generate-403-edn-response (str "Account NOT created successfully: " res))))
      (catch ExecutionException e
        (generate-403-edn-response
          (str "Exception creating account: " (.getMessage e)))))
    (generate-403-edn-response (str "Account already exists for " username "!"))))

(defn- read-account
  "Read an account with the given unique username.

  Evaluates to an EDN response."
  [id type]
  (if id
    (let [acct (read-account-entity id type)]
      (if (empty? acct)
        (generate-403-edn-response (str "Account NOT found for " id))
        (generate-edn-response acct)))
    (generate-403-edn-response "Username/EID not supplied!")))

(defn- read-accounts
  "Read all accounts.

  Evaluates to an EDN response."
  []
  (generate-edn-response
    {:message  "Accounts found."
     :accounts (read-account-entities)}))

(defn- read-accounts-history
  []
  "Read all account history.

  Evaluates to an EDN response."
  (generate-edn-response
    {:message          "Account history."
     :accounts-history (read-account-entities-history)}))

(defn- update-account
  "Update an account. Only the password can be changed by the user.  Most
  information is stored in the profile.

  Evaluates to an EDN response."
  [username password]
  (if-let [eid (ffirst (get-by-val :account/user username))]
    (try
      (let [res @(update-account-entity eid password)]
        (if (contains? res :tx-data)
          (generate-edn-response "Account updated")
          (generate-403-edn-response (str "Account NOT updated: " res))))
      (catch ExecutionException e
        (generate-403-edn-response
          (str "Exception updating account: " (.getMessage e)))))
    (generate-403-edn-response (str "Account not found for " username "!"))))

(defn- update-account-id [id nid]
  "Update an account id.

  Evaluates to an EDN response."
  (if (or (= "root" id) (= "root" nid))
    (generate-403-edn-response "root username cannot be changed from/to")
    (let [eid (ffirst (get-by-val :account/user id))
          neid (ffirst (get-by-val :account/user nid))]
      (if (and eid (not neid))
        (let [res @(update-account-entity-id eid id nid)]
          (if (contains? res :tx-data)
            (generate-edn-response (str "Account username changed" res))
            (generate-403-edn-response "Account username NOT changed!")))
        (generate-403-edn-response "Unable to update username.")))))

(defn- delete-account
  "Delete an account with the given unique username.

  Evaluates to an EDN response."
  [username]
  (if-let [eid (ffirst (get-by-val :account/user username))]
    (try
      (let [res @(delete-account-entity eid)]
        (if (contains? res :tx-data)
          (generate-edn-response "Account deleted")
          (generate-403-edn-response (str "Account NOT deleted: " res))))
      (catch ExecutionException e
        (generate-403-edn-response
          (str "Exception deleting account: " (.getMessage e)))))
    (generate-403-edn-response (str "Account not found for " username "!"))))

(defroutes account-routes
           (POST "/:username" [username password]
                 (authorize #{:role/admin} (create-account username password)))
           (GET "/" []
                (authorize #{:role/admin} (read-accounts)))
           (GET "/history" []
                (authorize #{:role/admin} (read-accounts-history)))
           (GET ["/:eid", :eid #"[0-9]+"] [eid]
                (authorize #{:role/admin} (read-account eid :eid)))
           (GET "/:username" [username]
                (authorize #{(keyword "role" username) :role/admin}
                           (read-account username :username)))
           (PUT "/:username" [username password]
                (authorize #{(keyword "role" username) :role/admin}
                           (update-account username password)))
           (PUT "/:username/:newusername" [username newusername]
                (authorize #{(keyword "role" username) :role/admin}
                           (update-account-id username newusername)))
           (DELETE "/:username" [username]
                   (authorize #{:role/admin} (delete-account username))))

(defroutes base-account-routes
           (context "/account" req account-routes))
