(ns glitter.entity.profile
  (:require [glitter.entity.util :refer [attr]]))

(defn profile-schema []
  (reduce into [(attr {:ident  :profile/id
                       :type   "ref"
                       :unique "identity"
                       :doc    "A profile id (refer to a user)"})
                (attr {:ident :profile/name
                       :doc   "A name"})
                (attr {:ident :profile/email
                       :doc   "An e-mail address"})
                (attr {:ident :profile/url
                       :doc   "A personal URL"})
                (attr {:ident :profile/company
                       :doc   "A company name"})
                (attr {:ident :profile/location
                       :doc   "A location"})]))

(defn profile-init []
  [;{:db/id #db/id[:db.part/user]
   ; :profile/id #db/id[:db.part/user -1000]
   ; :profile/name "root"
   ; :profile/email "root@glitter.com"}
    ])