(ns glitter.entity.util
  (:require [datomic.api :refer [connect create-database db delete-database
                                 entid entity history q tempid touch transact]]
            [environ.core :refer [env]]))

(def uri ^{:doc "Datomic environment specific URI"}
  (env :datomic-url))

(defn create-db
  "Create the Datomic database.

  Evalutes to true if the db was created, false otherwise."
  []
  (create-database uri))

(defn delete-db
  "Delete the Datomic database.

  Evaluates to true if the db was deleted, false otherwise."
  []
  (delete-database uri))

(def ^{:doc "first transducer"}
  first-xform (map first))

(defn entity-xform
  "Datomic entity transducer"
  [db]
  (map (partial entity db)))

(def touch-xform
  (map touch))

(defn fe-xform [db]
  (comp first-xform (entity-xform db)))

(defn fet-xform [db]
  (comp first-xform (entity-xform db) touch-xform))

(defn txn [t]
  (transact (connect uri) t))

(defn get-by-eid [eid]
  (q '[:find ?eid :in $ ?eid :where [?eid]] (db (connect uri)) eid))

(defn em->m [db em]
  (into {}
        (for [[k v] em]
          (if (:db/id v)
            [k (into {} (fet-xform db) (get-by-eid (:db/id v)))]
            [k v]))))

(defn em->m-xform [db]
  (comp (fet-xform db) (map (partial em->m db))))

(defn entmap->map [e]
  (into [] (em->m-xform (db (connect uri))) e))

(defn xforms [e & x]
  (into [] (comp (em->m-xform (db (connect uri))) (apply comp x)) e))

(defn attr
  "Define a Datomic attribute

  ident is required.  type and card are required, but default to 'string' and
  'one' respectively.

  ident - namespaced keyword
  type - one of the supported datomic value types as a string, i.e. 'string',
  'bigint','bytes', etc.
  card - cardinality.  'one' or 'many' is supported.
  doc - documentation for the attribute
  unique - Is this attribute unique.  'identity' or 'value' is supported.
  ind - Add an index to the attribute.  true is supported.
  ft - Add fulltext search support to the attribute.  true is supported.
  comp - Add isComponent to the attribute.  See the Datomic documentation for
  usage. true is supported.
  nh - Add noHistory to the attribute.  See the Datomic documentation for usage.
  true is supported."
  [{:keys [ident type card doc unique ind ft comp nh]
    :or   {type "string" card "one" ind false
           ft   false comp false nh false}}]
  (let [id (tempid :db.part/db)
        add [:db/add id]]
    (->> [(conj add :db/ident ident)
          (conj add :db/valueType (keyword "db.type" type))
          (conj add :db/cardinality (keyword "db.cardinality" card))
          (if (string? doc) (conj add :db/doc doc))
          (if unique (conj add :db/unique (keyword "db.unique" unique)))
          (if ind (conj add :db/index true))
          (if ft (conj add :db/fulltext true))
          (if comp (conj add :db/isComponent true))
          (if nh (conj add :db/noHistory true))
          [:db/add :db.part/db :db.install/attribute id]]
         (remove nil?)
         (into []))))

(defn add-enum [part kw]
  [[:db/add (tempid part) :db/ident kw]])

(defn get-by-attr [attr]
  (q '[:find ?e :in $ ?attr :where [?e ?attr]] (db (connect uri)) attr))

(defn get-by-val [attr val]
  (let [db (db (connect uri))]
    (q '[:find ?e :in $ ?attr ?val :where [?e ?attr ?val]]
       db (entid db attr) val)))

(defn get-by-ref [attr ref]
  (let [db (db (connect uri))]
    (get-by-val attr (entid db ref))))

(defn history-by-attr [attr]
  (let [hist (history (db (connect uri)))]
    (sort-by first
             (q '[:find ?tx ?v ?op ?e
                  :in $ ?attr
                  :where [?e ?attr ?v ?tx ?op]]
                hist
                attr))))