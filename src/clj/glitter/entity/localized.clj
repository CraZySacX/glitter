(ns glitter.entity.localized
  (:require [datomic.api :refer [connect db q]]
            [glitter.entity.util :refer [attr uri]]))

(defn localized-schema []
  (reduce into [(attr {:ident  :localized/id
                       :type   "ref"
                       :unique "identity"
                       :doc    "The unique id for a set of localized strings"})
                (attr {:ident :localized/brand
                       :doc   "Localized brand string"})
                (attr {:ident :localized/login
                       :doc   "Localized login string"})
                (attr {:ident :localized/logout
                       :doc   "Localized logout string"})
                (attr {:ident :localized/login-success
                       :doc   "Localized login success message"})
                (attr {:ident :localized/login-failure
                       :doc   "Localized login failure message"})
                (attr {:ident :localized/logout-success
                       :doc   "Localized logout sucess message"})
                (attr {:ident :localized/unauthenticated
                       :doc   "Localized unauthenticated message"})
                (attr {:ident :localized/unauthorized
                       :doc   "Localized unauthorized message"})
                (attr {:ident :localized/admin
                       :doc   "Localized admin string"})
                (attr {:ident :localized/account
                       :doc   "Localized account string"})]))

(defn localized-init []
  [{:db/id                     #db/id[:db.part/user]
    :localized/id              :locale.id/en_US
    :localized/brand           "Glitter"
    :localized/login           "Login"
    :localized/logout          "Logout"
    :localized/login-success   "Login Successful!"
    :localized/login-failure   "Login Failure! Invalid username/password."
    :localized/logout-success  "Logout Successful!"
    :localized/unauthenticated "Unauthenticated! Please login to access the requested resource."
    :localized/unauthorized    "Unauthorized! Your role does not have permission to access the requested resource."
    :localized/admin           "Admin"
    :localized/account         "Account"}
   {:db/id                     #db/id[:db.part/user]
    :localized/id              :locale.id/ru_RU
    :localized/brand           "блеск"
    :localized/login           "Войти"
    :localized/logout          "Logout"
    :localized/login-success   "Login Successful!"
    :localized/login-failure   "Login Failure! Invalid username/password."
    :localized/logout-success  "Logout Successful!"
    :localized/unauthenticated "Unauthenticated! Please login to access the requested resource."
    :localized/unauthorized    "Unauthorized! Your role does not have permission to access the requested resource."
    :localized/admin           "Admin"
    :localized/account         "Account"}])

(defn get-localized-eid [locale]
  (ffirst
    (q '[:find ?e
         :in $ ?l
         :where [?e :localized/id ?l]]
       (db (connect uri)) locale)))