(ns glitter.entity.account
  "account Datomic entity interaction"
  (:require [cemerick.friend.credentials :refer [hash-bcrypt]]
            [glitter.entity.util :refer :all]))

(def ^{:private true :doc "account password dissoc transducer"}
  pw-dis-xform (map #(dissoc % :account/password)))

(defn account-schema
  "account Datomic schema.

  Evaluates to a list of lists for use in transact."
  []
  (reduce into
          [(attr {:ident  :account/user
                  :unique "identity"
                  :doc    "A unique username"})
           (attr {:ident :account/password
                  :doc   "A password"})
           (attr {:ident :account/role
                  :type  "ref"
                  :card  "many"
                  :doc   "A role the account has"})
           (add-enum :db.part/user :role/admin)
           (add-enum :db.part/user :role/user)
           (add-enum :db.part/user :role/root)
           (add-enum :db.part/user :role/testuser)]))

(defn account-init
  "app-state initial Datomic attributes."
  []
  [{:db/id            #db/id[:db.part/user -1000]
    :account/user     "root"
    :account/password (hash-bcrypt "toor")
    :account/role     [:role/admin :role/user :role/root]}
   {:db/id            #db/id[:db.part/user -1001]
    :account/user     "testuser"
    :account/password (hash-bcrypt "testuser")
    :account/role     [:role/user :role/testuser]}])

(defn create-account-entity
  "Create an account entity.

  Roles should be a vector of role ident values (i.e. :role/user)"
  [username password]
  (let [urole (keyword "role" username)
        roles [:role/user urole]
        add [:db/add #db/id[:db.part/user -1]]
        btxn [(conj add :account/user username)
              (conj add :account/password (hash-bcrypt password))]]
    (txn (add-enum :db.part/user urole))
    (->> (for [role roles] (conj add :account/role role))
         (reduce conj btxn)
         (txn))))

(defn read-account-entity
  "Get the account entity for the given username"
  [id type]
  (let [dbfn (if (= type :username)
               (get-by-val :account/user id)
               (get-by-eid (read-string id)))]
    (xforms dbfn pw-dis-xform)))

(defn read-account-entities-history
  "Read all account entity history"
  []
  (history-by-attr :account/user))

(defn read-account-entities
  "Get all account entities"
  []
  (xforms (get-by-attr :account/user) pw-dis-xform))

(defn update-account-entity
  "Update an account entity identified by the given eid.

  Only password can be updated via this function."
  [eid password]
  (txn [[:db/add eid :account/password (hash-bcrypt password)]]))

(defn update-account-entity-id [eid id nid]
  (txn [[:db.fn/cas eid :account/user id nid]]))

(defn delete-account-entity
  "Retract an account entity with the given eid."
  [eid]
  (txn [[:db.fn/retractEntity eid]]))

(defn uem->um
  "Convert the user entity map from Datomic into a map that friend can use
  during authentication."
  []
  (let [accts (for [acct (entmap->map (get-by-attr :account/user))]
                {(:account/user acct) {:username (:account/user acct)
                                       :password (:account/password acct)
                                       :roles    (:account/role acct)}})]
    (if-not (empty? accts)
      (reduce into accts)
      {})))