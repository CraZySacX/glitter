(ns glitter.entity.core
  (:require [glitter.entity.account :refer [account-init
                                            account-schema]]
            [glitter.entity.app-state :refer [app-state-init
                                              app-state-schema]]
            [glitter.entity.localized :refer [localized-init
                                              localized-schema]]
            [glitter.entity.profile :refer [profile-init
                                            profile-schema]]
            [glitter.entity.util :refer [create-db delete-db txn uri]]
            [org.ozias.cljlibs.logging.logging :refer :all]))

(defn schema []
  (->> [(account-schema)
        (profile-schema)
        (app-state-schema)
        (localized-schema)]
       (reduce into)
       (txn)))

(defn initial []
  (txn (account-init))
  (txn (profile-init))
  (txn (localized-init))
  (txn (app-state-init)))

(defn start-datomic []
  (reportc :green :bold "Starting fresh Datomic at:   "
           :yellow :bold uri)
  (debugc :green "Deleting database:           "
          :yellow (delete-db))
  (debugc :green "Creating database:           "
          :yellow (create-db))
  (debugc :green "Creating schema:             "
          :yellow (:db-after @(schema)))
  (debugc :green "Creating initial attributes: "
          :yellow (:db-after @(initial))))

(defn stop-datomic []
  (reportc :green :bold "Stopping Datomic at: " :yellow :bold uri)
  (debugc :green "Deleting database:   " :yellow (delete-db)))
