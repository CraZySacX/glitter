(ns glitter.entity.app-state
  "app-state Datomic entity interaction"
  (:require [datomic.api :refer [connect db q]]
            [glitter.entity.localized :refer [get-localized-eid]]
            [glitter.entity.util :refer :all]))

(defn app-state-schema
  "app-state Datomic schema.

  Evaluates to a list of lists for use in transact."
  []
  (reduce into [(attr {:ident  :app-state/locale
                       :type   "ref"
                       :unique "identity"
                       :doc    "The unique locale for this app-state"})
                (attr {:ident :app-state/localized
                       :type  "ref"
                       :comp  true
                       :doc   "The localized strings for this locale"})
                (add-enum :db.part/user :locale.id/en_US)
                (add-enum :db.part/user :locale.id/ru_RU)]))

(defn app-state-init
  "app-state initial Datomic attributes."
  []
  [{:db/id               #db/id[:db.part/user]
    :app-state/locale    :locale.id/en_US
    :app-state/localized (get-localized-eid :locale.id/en_US)}
   {:db/id               #db/id[:db.part/user]
    :app-state/locale    :locale.id/ru_RU
    :app-state/localized (get-localized-eid :locale.id/ru_RU)}])

(defn app-state-xform
  "app-state default transducer. assoc's application version and friend identity
  into the app-state map."
  [version id]
  (map #(assoc % :app-state/version version :app-state/identity id)))

(defn get-app-state
  "Get the app-state with the given version and identity for the given locale.

  If locale is not given, :locale.id/en_US is assumed."
  [version id & locale]
  (let [locale (or (first locale) :locale.id/en_US)]
    (xforms (get-by-ref :app-state/locale locale)
            (app-state-xform version id))))

