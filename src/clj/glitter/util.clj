(ns glitter.util
  (:require [environ.core :refer [env]]
            [ring.util.response :refer [content-type response status]]
            [taoensso.timbre :refer [stacktrace]]))

(def ^{:doc "logging options"}
  logging-options
  {:stdout    true
   :logfile   (str (env :home) "/log/glitter.log")
   :formatter (fn [{:keys [throwable message]}]
                (format "%s%s" (or message "")
                        (or (stacktrace throwable \newline) "")))})

(defn generate-edn-response [data & [st]]
  (-> (response (pr-str data))
      (content-type "application/edn")
      (status (or st 200))))

(defn generate-403-edn-response [msg]
  (generate-edn-response {:message msg} 403))