(ns glitter.core
  (:require [cemerick.friend :as friend]
            [cemerick.friend.credentials :as creds]
            [cemerick.friend.workflows :as workflows]
            [clojure.tools.cli :refer [parse-opts]]
            [compojure.core :refer [ANY GET POST defroutes routes]]
            [compojure.handler :as handler]
            [compojure.route :refer [files]]
            [environ.core :refer [env]]
            [glitter.account :refer [base-account-routes]]
            [glitter.admin :refer [base-admin-routes]]
            [glitter.app-state :refer [base-app-state-routes]]
            [glitter.entity.account :refer [uem->um]]
            [glitter.entity.core :refer [start-datomic]]
            [glitter.git :refer [base-git-routes]]
            [glitter.util :refer :all]
            [glitter.version :refer [version]]
            [org.ozias.cljlibs.logging.logging :refer [configure-logging
                                                       errorc reportc]]
            [ring.adapter.jetty :refer [run-jetty]]
            [ring.middleware.edn :refer [wrap-edn-params]]
            [ring.util.response :refer [file-response]]
            [taoensso.timbre :refer [stacktrace]]))

(do
  (configure-logging {:options (assoc logging-options :verbosity 2)})
  (if (env :create) (start-datomic)))

(def ^:private cl-options
  [["-h" "--help" "Display this help information"]
   [nil "--version" "Display the glitter version" :default false]
   ["-v" nil "Verbosity level; may be specified multiple times to increase value"
    :id :verbosity
    :default 0
    :assoc-fn (fn [m k _] (update-in m [k] inc))]
   ["-r" "--repl" "Specify this option when running from a REPL"]
   ["-p" "--port PORT" "Port for the Jetty server to listen on"
    :default 3000
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
   [nil "--logfile LOGFILE" "Log file location"
    :default (str (env :home) "/log/glitter.log")]])

(defn- error-msg
  "generate an error message string"
  [config]
  (apply conj (:error config) (interpose \newline (:errors config))))

(defn exit
  "log the exit code and any associated messages"
  [exit-code & messages]
  (if (seq messages)
    (let [messages (remove nil? messages)
          messages (if (= \newline (last messages))
                     (butlast messages) messages)]
      (apply reportc messages)))
  exit-code)

(defn index []
  (file-response "public/html/index.html" {:root "resources"}))

(defn auth-failure-handler [msg _]
  (generate-edn-response {:message msg} 401))

(defroutes auth-routes
           (POST "/login" []
                 (generate-edn-response {:auth    true
                                         :message :localized/login-success}))
           (friend/logout
             (ANY "/logout" []
                  (generate-edn-response {:auth    false
                                          :message :localized/logout-success}))))
(defroutes base-routes
           (GET "/" [] (index))
           (files "/" {:root "resources/public"}))

(def app
  (-> (routes base-routes
              auth-routes
              base-admin-routes
              base-account-routes
              base-app-state-routes
              base-git-routes)
      (friend/authenticate {:allow-anon?             true
                            :redirect-on-auth?       false
                            :login-failure-handler   (partial auth-failure-handler :localized/login-failure)
                            :unauthenticated-handler (partial auth-failure-handler :localized/unauthenticated)
                            :unauthorized-handler    (partial auth-failure-handler :localized/unauthorized)
                            :default-landing-uri     "/"
                            :credential-fn           (partial creds/bcrypt-credential-fn (uem->um))
                            :workflows               [(workflows/interactive-form :redirect-on-auth? false)]})
      wrap-edn-params
      handler/site))

(defn- gen-config [args]
  (-> (parse-opts args cl-options)
      (update-in [:options] merge logging-options)
      (configure-logging)))

(defn run [& args]
  "Processing starts here.  Use this function when running from a REPL.

  The help option and any errors are handled here.  Otherwise, processing
  is passed to the argument handler."
  (let [config (gen-config args)]
    (cond
      (-> config :options :help) (apply exit 0 :green :bold (:summary config))
      (-> config :options :version) (apply exit 0 :green :bold version)
      (:errors config) (apply exit 1 :red :bold (error-msg config))
      :else (run-jetty app {:port 3000 :join? false}))))

(defn -main
  "Entry point for command-line processing.  This will be used when running
  outside of a REPL."
  [& args]
  (System/exit (apply run args)))