(ns glitter.admin
  (:require [cemerick.friend :as friend]
            [compojure.core :refer [context defroutes DELETE GET POST PUT]]
            [ring.util.response :refer [response]]))

(defroutes admin-routes
           (GET "/" [] (response "Admin Stuff")))

(defroutes base-admin-routes
           (context "/admin" [] (friend/wrap-authorize admin-routes #{::admin})))
