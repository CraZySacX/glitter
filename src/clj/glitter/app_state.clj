(ns glitter.app-state
  (:require [cemerick.friend :as friend]
            [compojure.core :refer [context defroutes DELETE GET POST PUT routes]]
            [datomic.api :as d]
            [glitter.util :refer :all]
            [glitter.entity.app-state :refer [get-app-state]]
            [glitter.version :refer [version]]))

(defn create-app-state [_]
  {:status 500})

(defn read-app-state [{:keys [locale] :or {locale "en_US"} :as req}]
  (let [locale (keyword "locale.id" locale)
        id (friend/identity req)]
    (-> (get-app-state version id locale)
        first
        generate-edn-response)))

(defn update-app-state [_]
  {:status 500})

(defn delete-app-state [_]
  {:status 500})

(defroutes app-state-routes
           (POST "/:locale" req (friend/authorize #{:role/admin} (create-app-state req)))
           (GET "/:locale" req (read-app-state req))
           (PUT "/:locale" req (friend/authorize #{:role/user} (update-app-state req)))
           (DELETE "/:locale" req (friend/authorize #{:role/admin} (delete-app-state req))))

(defroutes base-app-state-routes
           (context "/app-state" req app-state-routes))
