(ns glitter.git
  (:require [cemerick.friend :as friend]
            [compojure.core :refer [context defroutes DELETE GET POST PUT]]))

(defroutes repo-routes
           (GET "/" req {:status 500})
           (PUT "/" req (friend/authorize #{::admin} {:status 500}))
           (POST "/" req (friend/authorize #{::admin} {:status 500}))
           (DELETE "/" req (friend/authorize #{::admin} {:status 500})))

(defroutes base-git-routes
           (context "/repo" req repo-routes))
