(ns user
  (:require [clojure.pprint :refer (pprint)]
            [clojure.java.io :as io]
            [clojure.repl :refer :all]
            [clojure.tools.namespace.repl :refer (refresh refresh-all)]
            [datomic.api :refer [connect db q transact]]
            [glitter.entity.core :refer :all]
            [glitter.entity.util :refer :all]
            [org.ozias.cljlibs.scm.core :refer :all]
            [org.ozias.cljlibs.scm.git :refer :all]
            [speclj.core :refer :all]
            [speclj.run.standard]))